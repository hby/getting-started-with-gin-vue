package main

import (
	"embed"
	"fmt"
	"github.com/gin-gonic/gin"
	"mime"
	"strings"
	"time"
)

// 使用 go:embed 注解，将文件内容嵌入到程序中
//go:embed frontend/dist/*
var static embed.FS

func main() {
	r := gin.Default()
	r.GET("/api/hello", func(c *gin.Context) { // 注册路由
		timeFormat := "2006-01-02 15:04:05"
		msg := fmt.Sprintf("Hello, current time is %s", time.Now().Format(timeFormat))
		c.JSON(200, gin.H{
			"message": msg,
		})
	})
	r.NoRoute(func(c *gin.Context) { // 当 API 不存在时，返回静态文件
		path := c.Request.URL.Path                                   // 获取请求路径
		s := strings.Split(path, ".")                                // 分割路径，获取文件后缀
		prefix := "frontend/dist"                                    // 前缀路径
		if data, err := static.ReadFile(prefix + path); err != nil { // 读取文件内容
			// 如果文件不存在，返回首页 index.html
			if data, err = static.ReadFile(prefix + "/index.html"); err != nil {
				c.JSON(404, gin.H{
					"err": err,
				})
			} else {
				c.Data(200, mime.TypeByExtension(".html"), data)
			}
		} else {
			// 如果文件存在，根据请求的文件后缀，设置正确的mime type，并返回文件内容
			c.Data(200, mime.TypeByExtension(fmt.Sprintf(".%s", s[len(s)-1])), data)
		}
	})
	_ = r.Run(":8080")
}
